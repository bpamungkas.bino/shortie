import os
from dotenv import load_dotenv

load_dotenv()


class Config:
    APP_DIR = os.path.abspath(os.path.dirname(__file__))
    UI_DIR = os.path.join(APP_DIR, 'templates')
    PROJECT_ROOT = os.path.abspath(os.path.join(APP_DIR, os.pardir))
    SECRET_KEY = os.environ.get('SECRET_KEY', 'SECRET')
    HTTPAUTH_USER = os.environ.get('API_HTTPAUTH_USER', 'bino')
    HTTPAUTH_PASS = os.environ.get('API_HTTPAUTH_PASS', 'bino')


class DevConfig(Config):
    pass


class ProdConfig(Config):
    pass


class RedisConfig:
    DB_HOST = 'localhost'
    DB_PORT = 6379
    DB_USER = ''
    DB_PASSWORD = ''


ENV_MAPPING = {
    'DEVELOPMENT': DevConfig,
    'PRODUCTION': ProdConfig
}

config = ENV_MAPPING[os.environ.get('API_ENV', 'DEVELOPMENT')]
storage = RedisConfig
