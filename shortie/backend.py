import os
import datetime
import hug
import falcon
import short_url

from urllib.parse import urlparse
from redis.exceptions import DataError

from . import db

from .config import config


def expand(url: str, redir: hug.types.smart_boolean = True):
    try:
        data = db.redis_storage.get(url)
        if redir:
            return hug.redirect.to(data["url"], falcon.HTTP_301)
        else:
            return data
    except DataError:
        return "Not found"


def shorten(url: str):
    try:
        data = db.redis_storage.find(url)
    except TypeError:
        id = db.redis_storage.get_autoinc()
        shortened = short_url.encode_url(id)
        data = db.redis_storage.add(
            id=id,
            url=url,
            shortened=shortened,
            tags=[get_domain(url)],
            creator="system",
            created=datetime.datetime.now().isoformat()
        )
    finally:
        return data


def search(url: str):
    try:
        data = db.redis_storage.find(url)
    except TypeError:
        data = []
    finally:
        return data


def get_domain(url: str):
    parsed_uri = urlparse(url)
    return "{uri.netloc}".format(uri=parsed_uri)


def get_ui():
    with open(os.path.join(config.UI_DIR, "main.html")) as doc:
        return doc.read()
