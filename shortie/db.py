import redis
import json

from .config import storage


class RedisStorage:

    def __init__(self, host, port, password=''):
        self.handle = redis.Redis(
            host=host,
            port=port,
            password=password
        )

    def add(self, id: int, url: str, shortened: str, **kwargs):
        data = {
            'id': id,
            'url': url,
            'alias': shortened
        }
        data.update(kwargs)

        self.handle.set(shortened, url)
        self.handle.set(url, json.dumps(data))

        return data

    def get(self, short_url: str):
        data = self.handle.get(short_url)
        return json.loads(self.handle.get(data))

    def find(self, url: str):
        data = self.handle.get(url)
        return json.loads(data)

    def get_autoinc(self):
        return self.handle.incr('tracker_id')


redis_storage = RedisStorage(storage.DB_HOST, storage.DB_PORT)
