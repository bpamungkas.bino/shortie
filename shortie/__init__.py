__version__ = '0.1.0'
import hug

from . import backend
from .config import config

router = hug.route.API(__name__)
api = hug.API(__name__)

authentication = hug.authentication.basic(
    hug.authentication.verify(config.HTTPAUTH_USER, config.HTTPAUTH_PASS))

router.get(
    "/",
    output=hug.output_format.html,
    requires=authentication
)(backend.get_ui)

router.get("/g/{url}")(backend.expand)

router.get(
    "/s",
    requires=authentication
)(backend.shorten)

router.get(
    "/q",
    requires=authentication
)(backend.search)

router.get(
    "/apidoc",
    requires=authentication
)(api.http.documentation)

@hug.not_found()
def not_found():
    return "Not found"
