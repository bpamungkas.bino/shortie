# Hub + Redis

## Dependensi
- Python 3
- [poetry](https://poetry.eustace.io/)
- Redis

## Setup

- clone repo ini
- jalankan `poetry install`
- jalankan `poetry shell`

## Running
- Development dengan autoreload: `hug -m shortie`
- Production: `gunicorn shortie:__hug_wsgi__`